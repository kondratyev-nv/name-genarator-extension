# Fake name generation extension for Google Chrome

[Name Generator on the Chrome Store](https://chrome.google.com/webstore/detail/name-generator/ldkklmldmddiahpjfablcmabdfhodpbi)

Easy way to generate fake names and save them for later use.

### Supported generators

* [Random User Generator](https://randomuser.me/)
* [Random Profile](http://randomprofile.com/)
* [uinames.com](https://uinames.com/)

### Release notes

#### 0.0.0.3
 
 - Add refresh on sex and country change
 - Add Random Profile and uinames.com generators
 - Disable saved name selection when no available

#### 0.0.0.2

 - Add error handling for API calls
 - Add persisting last form state
 - Fix profile saving after manual changing form fields
 - Replace buttons text with icons

#### 0.0.0.1

 - Initial release
 